/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.util;

import com.google.gson.Gson;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Logan
 */
public class FlowerOrderFileUtility {
    
    /**
     * Checks that directory exists and creates if does not exist.
     * Also creates file conditional that it does not current exist.
     * 
     * @author Logan MacKinnon
     * @since 20200912
     * 
     * @param fileName
     * @return file
     */
    public static File setupFile(String fileName){
        File file = new File(fileName);
        File directory = new File(file.getParent());
        //Check that directory and file do/do not exist
        if(!directory.exists()){
            directory.mkdirs();
        }
        try {
            if(file.createNewFile()){
                System.out.println("New File Created at " + fileName);
            }
        } catch (IOException ex) {
            Logger.getLogger(FlowerOrderFileUtility.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return file;
    }
    
    /**
     * Method allows user to write ArrayList of orders
     * to a file in JSON format
     * 
     * @author LRM
     * @since 20201029
     * 
     * @param orders
     * @param file
     * @return Boolean
     */
    public static boolean write(ArrayList<FlowerOrder> orders, File file){
        //Setup file
        //File file = setupFile(FILE_PATH);
        
        FileWriter writer = null;
        Gson gson = new Gson();
        try{
            writer = new FileWriter(file, true);
            BufferedWriter bw = new BufferedWriter(writer);
            for(FlowerOrder current : orders){
                bw.write(gson.toJson(current) + "\n");
            }
            bw.close();
        }catch(IOException ioe){
            System.out.println("Exception caught: " + ioe.getMessage());
            return false;
        }finally{
            try{
                writer.close();
            }catch(IOException ioe){
                System.out.println("Exception caught: " + ioe.getMessage());
            }
        }
        return true;        
    }
}
