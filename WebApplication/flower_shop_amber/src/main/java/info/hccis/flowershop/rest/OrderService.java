/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.flowershop.rest;

import com.google.gson.Gson;
import info.hccis.flowershop.entity.jpa.FlowerOrder;
import info.hccis.flowershop.repositories.FlowerOrderRepository;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Optional;
import javax.ws.rs.PathParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author LRM
 * @since 20201116
 */
@Path("/OrderService/orders")
public class OrderService {
    
    private final FlowerOrderRepository fr;
    
    @Autowired
    public OrderService(FlowerOrderRepository fr){
        this.fr = fr;
    }
    
    @GET
    @Produces("application/json")
    public ArrayList<FlowerOrder> getAll(){
        ArrayList<FlowerOrder> orders = (ArrayList<FlowerOrder>) fr.findAll();
        return orders;
    }
    
    @GET
    @Path("/{id}")
    @Produces("application/json")
    public Response getFlowerOrderById(@PathParam("id") Integer id) throws URISyntaxException{
        Optional<FlowerOrder> order = fr.findById(id);
         if (!order.isPresent()) {
            return Response.status(204).build();
        } else {
            return Response
                    .status(200)
                    .entity(order).build();
        }
    }
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createFlowerOrder(String orderJson) 
    {        
        try{
            String temp = save(orderJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }
    }
    
    @PUT
    @Path("/{id}")
    @Consumes("application/json")
    @Produces("application/json")
    public Response updateOrder(@javax.ws.rs.PathParam("id") int id, String orderJson) throws URISyntaxException 
    {

        try{
            String temp = save(orderJson);
            return Response.status(201).entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();        
        }catch(AllAttributesNeededException aane){
            return Response.status(400).entity(aane.getMessage()).build();
        }

    }
    
    public String save(String json) throws AllAttributesNeededException{
        
        Gson gson = new Gson();
        FlowerOrder order = gson.fromJson(json, FlowerOrder.class);
        
        if(order.getCustomerId() == null) {
            throw new AllAttributesNeededException("Please provide all mandatory inputs");
        }
 
        if(order.getId() == null){
            order.setId(0);
        }

        order = fr.save(order);

        String temp = "";
        temp = gson.toJson(order);

        return temp;
        
        
    }
}
