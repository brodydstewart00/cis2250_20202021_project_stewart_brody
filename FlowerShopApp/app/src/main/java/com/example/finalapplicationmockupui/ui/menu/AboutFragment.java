package com.example.finalapplicationmockupui.ui.menu;

import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.example.finalapplicationmockupui.R;

/*
 * @author BDStewart @since 2021-03-24
 * Displays information about the flower shop, allows users to add our contact information to their contacts
 */
public class AboutFragment extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_about, container, false);
        Button contactButton = root.findViewById(R.id.contactButton);
        contactButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Creates a new Intent to insert a contact
                Intent insertContactIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
                // Sets the MIME type to match the Contacts Provider
                insertContactIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);
                // Inserts the contact for Brody's Bouquets
                insertContactIntent.putExtra(ContactsContract.Intents.Insert.EMAIL, "BrodysBouquets@gmail.com")
                    .putExtra(ContactsContract.Intents.Insert.EMAIL_TYPE,
                            ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                    .putExtra(ContactsContract.Intents.Insert.PHONE, "902-333-9191")
                    .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE,
                            ContactsContract.CommonDataKinds.Phone.TYPE_WORK)
                    .putExtra(ContactsContract.Intents.Insert.NAME, "Brody's Bouquets")
                    .putExtra(ContactsContract.Intents.Insert.POSTAL, "140 Weymouth St, Charlottetown, PE C1A 4Z1")
                    .putExtra(ContactsContract.Intents.Insert.NOTES, "Best flower shop in charlottetown");;
                startActivity(insertContactIntent);
            }
        });
        return root;
    }
}
