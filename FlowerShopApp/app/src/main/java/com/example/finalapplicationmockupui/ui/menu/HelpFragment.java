package com.example.finalapplicationmockupui.ui.menu;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.finalapplicationmockupui.R;

/*
 * @author BDStewart @since 2021-03-24
 * Allows users to view the README from the BitBucket repository.
 */
public class HelpFragment extends Fragment {
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_help, container, false);

        Button readMeButton = root.findViewById(R.id.btnOpenReadMe);
        readMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Creates an activity that opens the bit bucket repo in a browser
                String url = "https://bitbucket.org/brodydstewart00/cis2250_20202021_project_stewart_brody/src/master/README.md";
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });
        return root;
    }
}
