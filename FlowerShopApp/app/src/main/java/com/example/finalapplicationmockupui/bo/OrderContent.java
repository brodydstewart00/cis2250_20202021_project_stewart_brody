package com.example.finalapplicationmockupui.bo;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.core.app.NotificationCompat;
import androidx.room.Room;

import com.example.finalapplicationmockupui.MainActivity;
import com.example.finalapplicationmockupui.R;
import com.example.finalapplicationmockupui.api.JsonOrderApi;
import com.example.finalapplicationmockupui.orders.FlowerOrder;
import com.example.finalapplicationmockupui.room.FlowerOrderDatabase;
import com.example.finalapplicationmockupui.room.RoomDataAccess;
import com.example.finalapplicationmockupui.ui.viewOrder.ViewOrderFragment;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.app.NotificationChannel.DEFAULT_CHANNEL_ID;

/*
 * @author BDStewart @since 2021-03-24
 * Allows for interaction between the application and the API
 */

public class OrderContent {

    public static boolean isAPIActive;
    public static final List<FlowerOrder> ORDERS = new ArrayList<FlowerOrder>();

    public static void loadOrders(Context context) {

        //Use Retrofit to connect to the service
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8081/api/OrderService/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();


        JsonOrderApi jsonOrderApi = retrofit.create(JsonOrderApi.class);

        //Create a list of orders.
        Call<List<FlowerOrder>> call = jsonOrderApi.getOrders();

        call.enqueue(new Callback<List<FlowerOrder>>() {
        FlowerOrderDatabase db = Room.databaseBuilder(context,
                FlowerOrderDatabase.class, "FlowerOrder").allowMainThreadQueries().build();
        RoomDataAccess room = db.roomDataAccess();

            @Override
            public void onResponse(Call<List<FlowerOrder>> call, Response<List<FlowerOrder>> response) {
                //Retrieve the response from the API
                List<FlowerOrder> orders;
                orders = response.body();

                //Update the ORDERS list using the response
                OrderContent.ORDERS.clear();
                OrderContent.ORDERS.addAll(orders);

                //Update the room database with the new data.
                room.clearTable();
                for (FlowerOrder order : OrderContent.ORDERS) {
                    room.insertOrder(order);
                }

                //Stop the spinner and update the RecyclerView
                ViewOrderFragment.StopSpinner();
                ViewOrderFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Display a toast notification
                Toast finishedLoadingOrders = Toast.makeText(context, "Finished loading orders", Toast.LENGTH_SHORT);
                finishedLoadingOrders.show();

                //Log the API response
                Log.d("BDS", "orders retrieved using the API - " + orders.size());
                isAPIActive = true;
            }

            @Override
            public void onFailure(Call<List<FlowerOrder>> call, Throwable t) {
                //Display a toast notification
                Toast finishedLoadingOrders = Toast.makeText(context, "API Connection down, orders loaded from room", Toast.LENGTH_SHORT);
                finishedLoadingOrders.show();

                //If the API call fails, load orders from ROOM.
                OrderContent.ORDERS.clear();
                OrderContent.ORDERS.addAll(room.getOrders());

                //Stop the spinner and update the RecyclerView
                ViewOrderFragment.StopSpinner();
                ViewOrderFragment.getRecyclerView().getAdapter().notifyDataSetChanged();

                //Log the failure
                Log.d("BDS", "api call failed, using room data - "+ORDERS.size()+" orders retrieved from room");
                isAPIActive = false;
            }
        });
    }

    public static void PostOrderToRest(FlowerOrder order, Context context) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://10.0.2.2:8081/api/OrderService/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        JsonOrderApi jsonOrderApi = retrofit.create(JsonOrderApi.class);

        Call<FlowerOrder> call = jsonOrderApi.postOrder(order);
        call.enqueue(new Callback<FlowerOrder>() {
            @Override
            public void onResponse(Call<FlowerOrder> call, Response<FlowerOrder> response) {
                Log.d("BDS", response.toString());

                //Create a notification confirming the creation of a new order.
                NotificationCompat.Builder builder = new NotificationCompat.Builder(context, DEFAULT_CHANNEL_ID)
                        .setSmallIcon(R.drawable.view_icon)
                        .setContentTitle("New Order Created")
                        .setContentText("A new Flower Order has been created - Customer ID: "+order.getCustomerId())
                        .setPriority(NotificationCompat.PRIORITY_DEFAULT);
            }

            @Override
            public void onFailure(Call<FlowerOrder> call, Throwable t) {
                Log.d("BDS", t.toString());
            }
        });

    }
}
