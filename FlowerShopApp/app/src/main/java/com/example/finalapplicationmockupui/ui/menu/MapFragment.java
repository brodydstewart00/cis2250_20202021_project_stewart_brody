package com.example.finalapplicationmockupui.ui.menu;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.finalapplicationmockupui.R;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

/*
 * @author BDStewart @since 2021-03-24
 * Opens a MapView and navigates the view to the pin set over Holland College
 */
public class MapFragment extends Fragment implements OnMapReadyCallback {
    public MapView mapView;
    public GoogleMap map;

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_maps, container, false);

        // Gets the MapView from the XML layout and creates it
        mapView = (MapView) root.findViewById(R.id.map);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        return root;
    }
    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;

        // Add a marker to our FlowerShop at the Price of Whales campus
        LatLng storeFront = new LatLng(46.239725566495146, -63.12014433067064);
        map.addMarker(new MarkerOptions()
                .position(storeFront)
                .title("Brody's Bouquets - Charlottetown Location"));
        map.moveCamera(CameraUpdateFactory.newLatLng(storeFront));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(storeFront, 15));
    }
    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
}
