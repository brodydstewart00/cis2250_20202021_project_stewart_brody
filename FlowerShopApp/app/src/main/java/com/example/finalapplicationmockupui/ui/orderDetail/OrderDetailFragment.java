package com.example.finalapplicationmockupui.ui.orderDetail;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import com.example.finalapplicationmockupui.R;
import com.example.finalapplicationmockupui.orders.FlowerOrder;
import com.google.gson.Gson;

/*
 * @author BDStewart @since 2021-03-24
 * Allows users to view all of the details on a specific order, opens from the recycler view
 */
public class OrderDetailFragment extends Fragment {
    private FlowerOrder flowerOrder;
    private TextView orderId;
    private TextView customerId;
    private TextView orderDate;
    private TextView item1, item2, item3, item4, item5, item6;
    private TextView orderStatus;
    private TextView totalCost;
    private TextView amountPaid;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            String orderJson = getArguments().getString("order");
            Gson gson = new Gson();
            flowerOrder = gson.fromJson(orderJson, FlowerOrder.class);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.order_details, container, false);

        //Initialize the fragment elements.
        orderId = view.findViewById(R.id.txtOrderId);
        customerId = view.findViewById(R.id.txtCustomerId);
        orderDate = view.findViewById(R.id.txtOrderDate);
        item1 = view.findViewById(R.id.txtItem1);
        item2 = view.findViewById(R.id.txtItem2);
        item3 = view.findViewById(R.id.txtItem3);
        item4 = view.findViewById(R.id.txtItem4);
        item5 = view.findViewById(R.id.txtItem5);
        item6 = view.findViewById(R.id.txtItem6);
        orderStatus = view.findViewById(R.id.txtOrderStatus);
        totalCost = view.findViewById(R.id.txtTotalCost);
        amountPaid = view.findViewById(R.id.txtAmountPaid);

        //Display details to the fragment.
        orderId.append(flowerOrder.getId().toString());
        customerId.append(flowerOrder.getCustomerId().toString());
        orderDate.append(flowerOrder.getOrderDate().toString());
        item1.append(flowerOrder.getItem1().toString());
        item2.append(flowerOrder.getItem2().toString());
        item3.append(flowerOrder.getItem3().toString());
        item4.append(flowerOrder.getItem4().toString());
        item5.append(flowerOrder.getItem5().toString());
        item6.append(flowerOrder.getItem6().toString());
        orderStatus.append((flowerOrder.getOrderStatus().toString()));
        totalCost.append(flowerOrder.getTotalCost().toString());
        amountPaid.append(flowerOrder.getAmountPaid().toString());

        //Display a message to the user
        Toast finishedLoadingOrders = Toast.makeText(view.getContext(), "Finished loading details for Order #"+flowerOrder.getId(), Toast.LENGTH_SHORT);
        finishedLoadingOrders.show();

        return view;
    }
}
