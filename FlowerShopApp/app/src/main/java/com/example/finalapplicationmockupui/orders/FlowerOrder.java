/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.finalapplicationmockupui.orders;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import com.example.finalapplicationmockupui.room.Converters;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.HashMap;

/*
 * @author BDStewart @since 2021-03-24
 * FlowerOrder object class
 */

@Entity
public class FlowerOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    private Integer id;

    @ColumnInfo(name = "customerId")
    private Integer customerId;

    @ColumnInfo(name = "orderDate")
    private String orderDate;

    @ColumnInfo(name = "item1")
    private Integer item1;

    @ColumnInfo(name = "item2")
    private Integer item2;

    @ColumnInfo(name = "item3")
    private Integer item3;

    @ColumnInfo(name = "item4")
    private Integer item4;

    @ColumnInfo(name = "item5")
    private Integer item5;

    @ColumnInfo(name = "item6")
    private Integer item6;

    @ColumnInfo(name = "orderStatus")
    private Integer orderStatus;

    @TypeConverters(Converters.class)
    @ColumnInfo(name = "totalCost")
    private BigDecimal totalCost;

    @TypeConverters(Converters.class)
    @ColumnInfo(name = "amountPaid")
    private BigDecimal amountPaid;

    public FlowerOrder() {
    }

    public FlowerOrder(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    public Integer getItem1() {
        return item1;
    }

    public void setItem1(Integer item1) {
        this.item1 = item1;
    }

    public Integer getItem2() {
        return item2;
    }

    public void setItem2(Integer item2) {
        this.item2 = item2;
    }

    public Integer getItem3() {
        return item3;
    }

    public void setItem3(Integer item3) {
        this.item3 = item3;
    }

    public Integer getItem4() {
        return item4;
    }

    public void setItem4(Integer item4) {
        this.item4 = item4;
    }

    public Integer getItem5() {
        return item5;
    }

    public void setItem5(Integer item5) {
        this.item5 = item5;
    }

    public Integer getItem6() {
        return item6;
    }

    public void setItem6(Integer item6) {
        this.item6 = item6;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public BigDecimal getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(BigDecimal totalCost) {
        this.totalCost = totalCost;
    }

    public BigDecimal getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(BigDecimal amountPaid) {
        this.amountPaid = amountPaid;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FlowerOrder)) {
            return false;
        }
        FlowerOrder other = (FlowerOrder) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    //Calculate the total cost of the order.
    public void calculate() {
        double cost;

        double cost1 = getItem1() * 90;
        double cost2 = getItem2() * 85;
        double cost3 = getItem3() * 100;
        double cost4 = getItem4() * 10;
        double cost5 = getItem5() * 20;
        double cost6 = getItem6() * 30;

        cost = cost1 + cost2 + cost3 + cost4 + cost5 + cost6;

        BigDecimal bdCost = new BigDecimal(cost, MathContext.DECIMAL64);
        totalCost = bdCost;
    }

    @Override
    public String toString() {
        return "info.hccis.flowershop.entity.FlowerOrder[ id=" + id + " ]";
    }
    
}
