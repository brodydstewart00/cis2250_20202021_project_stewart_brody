package com.example.finalapplicationmockupui.ui.addOrder;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.fragment.app.Fragment;
import com.example.finalapplicationmockupui.R;
import com.example.finalapplicationmockupui.api.JsonOrderApi;
import com.example.finalapplicationmockupui.bo.OrderContent;
import com.example.finalapplicationmockupui.orders.FlowerOrder;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Calendar;
import java.util.Locale;
import static androidx.core.content.ContextCompat.getSystemService;

/*
 * @author BDStewart @since 2021-03-24
 * Fragment that allows the user to create a new order, posts it using the web-services
 */
public class AddOrderFragment extends Fragment {

    private JsonOrderApi api;
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_add, container, false);

        //Initialize Fragment Elements
        EditText customerId = root.findViewById(R.id.inputCustomerID);
        Spinner babyBoy = root.findViewById(R.id.inputBabyBoy);
        Spinner babyGirl = root.findViewById(R.id.inputBabyGirl);
        Spinner verySpecial = root.findViewById(R.id.inputSpecialDeli);
        Spinner stuffedSmall = root.findViewById(R.id.inputSmall);
        Spinner stuffedMedium = root.findViewById(R.id.inputMedium);
        Spinner studdedLarge = root.findViewById(R.id.inputLarge);
        EditText amountPaid = root.findViewById(R.id.inputAmountPaid);

        //Create Order Listener
        Button addOrderButton = root.findViewById(R.id.createOrderButton);
        addOrderButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(OrderContent.isAPIActive) {

                    //Create order, and get user input from fragment
                    FlowerOrder order = new FlowerOrder();
                    order.setCustomerId(Integer.parseInt(customerId.getText().toString()));
                    order.setItem1(Integer.parseInt(babyBoy.getSelectedItem().toString()));
                    order.setItem2(Integer.parseInt(babyGirl.getSelectedItem().toString()));
                    order.setItem3(Integer.parseInt(verySpecial.getSelectedItem().toString()));
                    order.setItem4(Integer.parseInt(stuffedSmall.getSelectedItem().toString()));
                    order.setItem5(Integer.parseInt(stuffedMedium.getSelectedItem().toString()));
                    order.setItem6(Integer.parseInt(studdedLarge.getSelectedItem().toString()));
                    order.setOrderStatus(1);
                    order.calculate();
                    Date today = Calendar.getInstance().getTime();
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
                    order.setOrderDate(format.format(today));
                    order.setAmountPaid(new BigDecimal(amountPaid.getText().toString()));

                    OrderContent.PostOrderToRest(order, getContext());

                    //Create notification channel
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
                        int importance = NotificationManager.IMPORTANCE_DEFAULT;
                        NotificationChannel channel = null;
                        channel = new NotificationChannel("display_to_user", "display", importance);

                        channel.setDescription("Display a notification to the user");
                        // Register the channel with the system; you can't change the importance
                        // or other notification behaviors after this
                        NotificationManager notificationManager = getSystemService(getContext(), NotificationManager.class);
                        notificationManager.createNotificationChannel(channel);
                    }

                    //Create notification
                    NotificationCompat.Builder builder = new NotificationCompat.Builder(getContext(), "display_to_user")
                            .setSmallIcon(R.drawable.flower)
                            .setContentTitle("New Order Created!")
                            .setContentText("A new order has been created at Brody's Bouquets! - Customer ID: " + order.getCustomerId())
                            .setStyle(new NotificationCompat.BigTextStyle())
                            .setPriority(NotificationCompat.PRIORITY_DEFAULT);

                    NotificationManagerCompat notificationManager = NotificationManagerCompat.from(getContext());

                    // notificationId is a unique int for each notification that you must define
                    notificationManager.notify(1, builder.build());
                }
                else
                {
                    //Notify the user that orders cannot be placed when the API is down.
                    Toast finishedLoadingOrders = Toast.makeText(root.getContext(), "Orders cannot be created while the API is down.", Toast.LENGTH_SHORT);
                    finishedLoadingOrders.show();
                }
            }
        });
        return root;
    }
}