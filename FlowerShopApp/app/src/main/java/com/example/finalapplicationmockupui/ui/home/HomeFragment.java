package com.example.finalapplicationmockupui.ui.home;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import com.example.finalapplicationmockupui.R;
import com.synnapps.carouselview.CarouselView;
import com.synnapps.carouselview.ImageListener;

/*
* @author BDStewart @since 2021-03-24
* Fragment that the app lands on, displays a carousel of bouquet images.
 */
public class HomeFragment extends Fragment {
    public CarouselView homePageCarousel;
    int[] bouquetImages = {R.drawable.image_1, R.drawable.image_2, R.drawable.image_3, R.drawable.image_4, R.drawable.image_5};
    public ImageListener imageListener = (position, imageView) -> imageView.setImageResource(bouquetImages[position]);

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //Setup Carousel
        homePageCarousel = root.findViewById(R.id.carouselView);
        homePageCarousel.setPageCount(bouquetImages.length);
        homePageCarousel.setImageListener(imageListener);
        return root;
    }


}