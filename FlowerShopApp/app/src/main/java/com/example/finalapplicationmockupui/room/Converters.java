package com.example.finalapplicationmockupui.room;

import androidx.room.TypeConverter;

import java.math.BigDecimal;
/*
 * @author BDStewart @since 2021-03-24
 * Converts BigDecimal to Long, allowing for Room Databases to store Big Decimals as long values
 */
public class Converters {
    @TypeConverter
    public BigDecimal fromLong(Long value) {
        return value == null ? null : new BigDecimal(value).divide(new BigDecimal(100));
    }

    @TypeConverter
    public Long toLong(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return null;
        } else {
            return bigDecimal.multiply(new BigDecimal(100)).longValue();
        }
    }
}
