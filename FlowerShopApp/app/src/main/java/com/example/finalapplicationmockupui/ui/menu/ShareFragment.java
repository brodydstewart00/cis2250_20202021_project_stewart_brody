package com.example.finalapplicationmockupui.ui.menu;

import android.Manifest;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import com.example.finalapplicationmockupui.MainActivity;
import com.example.finalapplicationmockupui.R;

/*
 * @author BDStewart @since 2021-03-24
 * Creates a twitter post using sample text
 */
public class ShareFragment extends Fragment {
    private Bitmap imageCaptured;
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_share, container, false);
        Button shareButton = root.findViewById(R.id.btnShare);
        Button pictureButton = root.findViewById(R.id.btnPicture);
        EditText shareText = root.findViewById(R.id.txtShareMessage);
        shareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Check if the user has the Twitter App installed

                try{
                    //Open twitter and share text/image
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_SEND);
                    intent.putExtra(Intent.EXTRA_TEXT, shareText.getText().toString());
                    intent.setType("text/plain");
                    if(imageCaptured != null) {
                        String path = MediaStore.Images.Media.insertImage(getActivity().getApplicationContext().getContentResolver(), imageCaptured, "Image Description", null);
                        Uri uri = Uri.parse(path);
                        intent.putExtra(Intent.EXTRA_STREAM, uri);
                        intent.setType("image/jpeg");
                    }
                    intent.setPackage("com.twitter.android");
                    startActivity(intent);

                } catch( ActivityNotFoundException activityNotFoundException ){
                    //Display a toast notification
                    Toast needTwitterInstalledToast = Toast.makeText(getContext(), "You need the Twitter app installed to use this feature.", Toast.LENGTH_SHORT);
                    needTwitterInstalledToast.show();
                }
            }
        });

        //Request Camera Permissions
        if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {Manifest.permission.CAMERA}, 100);
        }

        pictureButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Open Camera
                Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, 100);
            }
        });
        return root;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==100) {
            try {
                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");
                imageCaptured = imageBitmap;
            } catch (NullPointerException e) {

            }
        }
    }

    public boolean isTwitterInstalled() {
        try{
            ApplicationInfo info = getActivity().getPackageManager().
                    getApplicationInfo("com.twitter.android", 0 );
            return true;
        } catch( PackageManager.NameNotFoundException e ){
            return false;
        }
    }
}