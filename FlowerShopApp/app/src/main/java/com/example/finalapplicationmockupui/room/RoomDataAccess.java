package com.example.finalapplicationmockupui.room;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.finalapplicationmockupui.orders.FlowerOrder;

import java.util.List;

import kotlinx.coroutines.flow.Flow;
/*
 * @author BDStewart @since 2021-03-29
 * Allows interaction with the data stored locally in Room
 */
@Dao
public interface RoomDataAccess {
    @Query("SELECT * FROM FlowerOrder")
    List<FlowerOrder> getOrders();

    @Insert
    void insertOrder(FlowerOrder order);

    @Query("DELETE FROM FlowerOrder")
    public void clearTable();
}
