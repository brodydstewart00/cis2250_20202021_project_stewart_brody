package com.example.finalapplicationmockupui.room;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.finalapplicationmockupui.orders.FlowerOrder;
/*
 * @author BDStewart @since 2021-03-24
 * Creates the FlowerOrder database in room
 */
@Database(entities = {FlowerOrder.class}, version = 1)
public abstract class FlowerOrderDatabase extends RoomDatabase {
    public abstract RoomDataAccess roomDataAccess();
}
