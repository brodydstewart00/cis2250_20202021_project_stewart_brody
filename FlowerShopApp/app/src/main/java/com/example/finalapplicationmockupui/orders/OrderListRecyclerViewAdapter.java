package com.example.finalapplicationmockupui.orders;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.example.finalapplicationmockupui.R;
import com.example.finalapplicationmockupui.bo.OrderContent;
import com.example.finalapplicationmockupui.ui.viewOrder.ViewOrderFragment;
import java.util.List;

/*
 * @author BDStewart @since 2021-03-24
 * RecyclerView adapter used to display FlowerOrder items
 */
public class OrderListRecyclerViewAdapter extends RecyclerView.Adapter<OrderListRecyclerViewAdapter.ViewHolder> {

    private final List<FlowerOrder> mValues;
    private final ViewOrderFragment.OnListFragmentInteractionListener mListener;

    public OrderListRecyclerViewAdapter(List<FlowerOrder> items, ViewOrderFragment.OnListFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.order_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.order = mValues.get(position);
        holder.m_CustomerID.setText("Customer ID: " +mValues.get(position).getCustomerId());
        holder.m_OrderDate.setText("Order Date: " + mValues.get(position).getOrderDate());
        holder.m_OrderTotal.setText("Order Total: $" + mValues.get(position).getTotalCost());

        holder.mView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("BDS", "clicked on a row of the recyclerview.  ");
                if (null != mListener) {
                    // Notify the active callbacks interface (the activity, if the
                    // fragment is attached to one) that an item has been selected.
                    mListener.onListFragmentInteraction(holder.order);
                } else {
                    Log.d("BDS", "Listener is Null");
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView m_CustomerID;
        public TextView m_OrderTotal;
        public TextView m_OrderDate;
        public FlowerOrder order;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            m_CustomerID = view.findViewById(R.id.customerID);
            m_OrderTotal = view.findViewById(R.id.orderTotal);
            m_OrderDate = view.findViewById(R.id.orderDate);
        }
    }

    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FlowerOrder item);
    }
}
