package com.example.finalapplicationmockupui.ui.viewOrder;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.room.Room;
import com.example.finalapplicationmockupui.R;
import com.example.finalapplicationmockupui.bo.OrderContent;
import com.example.finalapplicationmockupui.orders.FlowerOrder;
import com.example.finalapplicationmockupui.orders.OrderListRecyclerViewAdapter;
import com.example.finalapplicationmockupui.room.FlowerOrderDatabase;
import com.example.finalapplicationmockupui.room.RoomDataAccess;

/*
 * @author BDStewart @since 2021-03-24
 * Displays a list of all orders from the web service
 */
public class ViewOrderFragment extends Fragment {

    private static RecyclerView recyclerView;
    private int m_ColumnCount;
    private OnListFragmentInteractionListener mListener;
    public static RecyclerView getRecyclerView() {
        return recyclerView;
    }
    private static ProgressBar spinner;

    //Create and inflate fragment view.
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_view, container, false);

        //Get data for display
        OrderContent.loadOrders(getContext());

        //Initialize the Loading Spinner
        spinner = (ProgressBar) root.findViewById(R.id.progressBar1);
        return root;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        //Start the loading spinner.
        spinner.setVisibility(View.VISIBLE);

        // Set the adapter
        Context context = getView().getContext();
        recyclerView = getView().findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager((context), LinearLayoutManager.VERTICAL, false));

        //Initialize Room
        FlowerOrderDatabase db = Room.databaseBuilder(getContext(), FlowerOrderDatabase.class, "FlowerOrder").allowMainThreadQueries().build();
        RoomDataAccess room = db.roomDataAccess();

        //Setup the RecyclerView
        recyclerView.setLayoutManager(new LinearLayoutManager((context), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new OrderListRecyclerViewAdapter(OrderContent.ORDERS, mListener));
    }

    //Stop the loading spinner
    public static void StopSpinner() {
        spinner.setVisibility(View.GONE);
    }


    public interface OnListFragmentInteractionListener {
        void onListFragmentInteraction(FlowerOrder item);
    }
}