package com.example.finalapplicationmockupui.api;

import com.example.finalapplicationmockupui.orders.FlowerOrder;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
/*
 * @author BDStewart @since 2021-03-24
 * API interaction interface
 */
public interface JsonOrderApi {
    String API_ROUTE = "orders";

    @GET(API_ROUTE)
    Call<List<FlowerOrder>> getOrders();

    @POST(API_ROUTE)
    Call<FlowerOrder> postOrder(@Body FlowerOrder order);
}
